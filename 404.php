<?php
/**
 * The template for displaying 404 pages (not found)
 *
 */

get_header(); ?>

<div class="container-small page-single-post">

	<div class="col-md-12 text-center">
		<h1 class="project-title"><?php _e( '404.php', 'stefandjakovic' ); ?></h1>
		<h1 class="project-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'stefandjakovic' ); ?></h1>
	</div>	
	<div class="col-md-12 text-center">
		<p><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'stefandjakovic' ); ?></p>
	</div>
	<div class="col-md-12 search-form text-center">
		<?php get_search_form(); ?>
	</div>
</div>


<?php get_footer();