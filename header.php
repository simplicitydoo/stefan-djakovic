<?php
/**
 *
 * @package WordPress
 * @subpackage Stefan Djakovic
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-12 logo">
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
						<div class="hide-on-desktop">
							<?php get_template_part( 'template-parts/header/social', 'top' ); ?>
						</div>
				</div>
				<div class="col-md-6 col-xs-12">
					<?php get_template_part( 'template-parts/navigation/navigation', 'top' ); ?>
				</div>
				<div class="col-md-3 col-xs-12 hide-on-mobile">
					<?php get_template_part( 'template-parts/header/social', 'top' ); ?>
				</div>
			</div>
		</div>
	</header>