<?php

function stefandjakovic_setup() {

	load_theme_textdomain( 'stefandjakovic' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'title-tag' );

	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'stefandjakovic' ),
		'social' => __( 'Social Links Menu', 'stefandjakovic' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
		'project',
	) );

	add_theme_support( 'menus');

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'stefandjakovic' )
	) );

	}
add_action( 'after_setup_theme', 'stefandjakovic_setup' );


function stefandjakovic_scripts() {

	// Theme stylesheet.
	wp_enqueue_style( 'stefandjakovic-style', get_stylesheet_uri() );

	// Custom Theme stylesheet.
	wp_enqueue_style( 'sdj-style', get_theme_file_uri( '/assets/css/stefandjakovic.css' ), array(), '1.0' );

	//Google Fonts
	wp_enqueue_style( 'sdj-google-fonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300', false ); 

	// Fancybox stylesheet.
	wp_enqueue_style( 'fancybox', get_theme_file_uri( '/assets/css/jquery.fancybox.css' ), array(), '1.0' );

	//jQuery
	wp_enqueue_script( 'jquery' );

	//Fancybox script
	wp_enqueue_script( 'fancybox', get_theme_file_uri( '/assets/js/jquery.fancybox.js' ), array(), '1.0', true );

	//Masonary script and Imagesloaded
	wp_enqueue_script( 'imagesloaded', get_theme_file_uri( '/assets/js/imagesloaded.pkgd.min.js' ), array( 'jquery' ), '4.1.1', true );
	wp_enqueue_script( 'masonary', get_theme_file_uri( '/assets/js/masonry.pkgd.min.js' ), array( 'jquery', 'imagesloaded' ), '4.1.1', true );

	//Custom js
	wp_enqueue_script( 'main', get_theme_file_uri( '/assets/js/main.js' ), array(), '1.0', true );
	wp_localize_script(
		'main',
		'stefan_ajax',
		array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'button'   => __( 'Loading...', 'stefandjakovic' )
		)
	);
}
add_action( 'wp_enqueue_scripts', 'stefandjakovic_scripts' );
if ( function_exists('acf_add_options_page') ) {

	acf_add_options_page (array(
		'page_title'	=> 'Overview',
		'menu_title'	=> 'Overview',
		'menu_slug'		=> 'add-overview',
		'parent_slug'	=> '',
		'positon'		=> false,
		'icon_url'		=> 'dashicons-visibility',
	));

	acf_add_options_page (array(
		'page_title'	=> 'Contact & Social Media',
		'menu_title'	=> 'Contact & Social Media',
		'menu_slug'		=> 'contact-social',
		'parent_slug'	=> '',
		'positon'		=> false,
		'icon_url'		=> 'dashicons-email-alt',
	));
}

//custom post type
function stefandjakovic_cpt_project () {
	$labels = array (
		'name' 				=> 'Project',
		'singular_name'		=> 'Project',
		'add_new'			=> 'Add New',
		'all_items'			=> 'All Items',
		'add_new_item'		=> 'Add Item',
		'edit_item'			=> 'Edit Item',
		'new_item'			=> 'New Item',
		'view_item'			=> 'View Item',
		'search_item'		=> 'Search Projects',
		'not_found'			=> 'No item found',
		'not_found_in_trash'=> 'No item found in trash',
		'parent_item_colon' => 'Parent Item'
	);
	$args = array (
		'labels'			=> $labels,
		'public'			=> true,
		'has_archive'		=> true,
		'publicly_queryable' => true,
		'query_var'			=> true,
		'capability_type'	=> 'post',
		'hierarchical'		=> false,
		'supports'			=> array (
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'revisions',
			'comments',
		),
		'taxonomies'		=> array (
			'category',
			'post_tag'
		),
		'menu_position'		=> 5,
		'exclude_from_search' => false
	);

	register_post_type( 'project', $args );
}

add_action( 'init', 'stefandjakovic_cpt_project' );


add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

/**
 * Overview AJAX
 */
function load_more_overview() {

	// Check the nonce, if ok, proceed.
	if ( ! wp_verify_nonce( $_REQUEST['nonce'], 'overview_nonce' ) ) {

		die;

	}

	// Get Offset
	if ( isset( $_REQUEST['offset'] ) ) {

		$offset = intval( $_REQUEST['offset'] );

	} else {

		die;

	}

	global $post;

	// Get our items
	$items = get_field( 'add_item', 'option' );

	if ( $offset != 0 ) {

		$items = array_slice( $items, $offset );

	}

	if ( ! empty( $items ) ) {

		$html = '';

		foreach ( $items as $item ) {

			$title = $item['overview_title'];
			$image = $item['overview_image'];
			$description = $item['overview_short_description'];
			$link = $item['link_to_the_project'];

			$html .= '<article class="masonry-item">';
				$html .= '<div class="item">';
					$html .= '<div class="image-container">';
						$html .= '<a href="' . esc_url( $image['link'] ) . '" class="fancybox" rel="gallery1" data-title="' . esc_attr( $title ) . '" data-description="' . esc_attr( $description ) . '" data-link="' . esc_url( $link ) . '">';
							$html .= '<img src="' . esc_url( $image['url'] ) . '" alt="' . esc_attr( $image['alt'] ) . '" />';
						$html .= '</a>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</article>';

		}

		$return = array(
			'has_more' => true,
			'html'     => json_encode( $html ),
			'offset'   => $offset + 5
		);

	} else {

		$return = array(
			'has_more' => false,
			'message'  => __( 'Done!', 'stefandjakovic' )
		);

	}

	return wp_send_json( $return );

}
add_action( 'wp_ajax_load_more_overview', 'load_more_overview' );
add_action( 'wp_ajax_nopriv_load_more_overview', 'load_more_overview' );