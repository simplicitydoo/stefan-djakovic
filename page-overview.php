<?php
/**
 * Template Name: Overview Page
 *
 * @package WordPress
 * @subpackage stefandjakovic
 * @since stefandjakovic 1.0
 */
?>

<?php get_header(); ?>
<div class="container page-overview">

	<?php
		$items = get_field( 'add_item', 'option' );
		$items = array_slice( $items, 0, 5 );
		if ( isset( $items ) && ! empty( $items ) ) {

			echo '<div class="masonry stefandjakovic_post_container">';
				foreach ( $items as $item ) {

					$title = $item['overview_title'];
					$image = $item['overview_image'];
					$description = $item['overview_short_description'];
					$link = $item['link_to_the_project'];

					?>
						<article id="post-<?php the_ID(); ?>" <?php post_class( 'masonry-item' ); ?>>
							<div class="item">

								<div class="image-container">
									<a href="<?php echo esc_url( $image['url'] ); ?>" class="fancybox" rel="gallery1" data-title="<?php echo esc_attr( $title ); ?>" data-description="<?php echo esc_attr( $description ); ?>" data-link="<?php echo esc_url( $link ); ?>">
										<img src="<?php echo esc_url( $image['url'] ); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" />
									</a>
								</div>

							</div>
						</article>
					<?php
				}
			echo '</div>';

		}
	?>
	<div class="col-md-12 text-center">
		<a class="loadmore-button load-more" data-nonce="<?php echo wp_create_nonce( 'overview_nonce' ); ?>" data-offset="5"><?php _e( 'LOAD MORE', 'stefandjakovic' ); ?></a>
	</div>
</div>
<?php get_footer(); ?>