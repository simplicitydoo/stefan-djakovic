<?php get_header(); ?>
	<div class="container page-single-project">
		
		<div class="masonry">
			<?php while ( have_posts() ) : the_post(); ?>
				<div class="item">
					<a href="<?php echo get_permalink(); ?>">
						<div class="image-container">
							<?php if ( has_post_thumbnail() ) : ?>
		        				<img src="<?php echo the_post_thumbnail_url(); ?>">
		    				<?php endif; ?>
						</div>
						<h1><?php the_title(); ?></h1>
					</a>
				</div>
			<?php endwhile; // End of the loop. ?>
		</div>	
	</div>
<?php get_footer(); ?>