<?php
/**
 * Template Name: Projects Page
 *
 * @package WordPress
 * @subpackage stefandjakovic
 * @since stefandjakovic 1.0
 */
?>

<?php get_header(); ?>
	<div class="container page-single-project">
		
		<div class="masonry">
			<?php 
				$args = array( 'post_type' => 'project');
				$loop = new WP_Query( $args );
				while ( $loop->have_posts() ) : $loop->the_post(); ?>
				<div class="item">
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<a href="<?php echo get_permalink(); ?>">
							<div class="image-container text-center">
								<?php if ( has_post_thumbnail() ) : ?>
			        				<img src="<?php echo the_post_thumbnail_url(); ?>">
			    				<?php endif; ?>
							</div>
							<h1 class="text-center"><?php the_title(); ?></h1>
						</a>
					</article>
				</div>
			<?php endwhile; // End of the loop. ?>
		</div>	
	</div>
<?php get_footer(); ?>