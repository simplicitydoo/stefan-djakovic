<?php 
/**
 * The template for displaying all single projects
 *
 */
get_header(); ?>
	<div class="container page-single-project">
		
	</div>
	<div class="container-small page-single-post">
		<?php while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>	
				<div class="text-center">
					<?php if ( has_post_thumbnail() ) : ?>
		        		<?php the_post_thumbnail(); ?>
		    		<?php endif; ?>
	    		</div>	
				<h1 class="text-center project-title"><?php the_title(); ?></h1>
				
				<?php the_content(); ?>
					
				<?php get_template_part( 'template-parts/page/project/share-project'); ?>
			</article>
			<div class="col-md-12">
					<?php
						if ( comments_open() || get_comments_number() ) :
	    				 	comments_template();
	 					endif;
					?>
				</div>		
			<div class="text-center prev-next-project"><?php previous_post_link( '%link', '<span class="icon-arrow_left"></span>  Prevoius project'); ?><span>   //   </span><?php next_post_link('%link', 'Next project  <span class="icon-arrow_right"></span>'); ?></div>

		<?php endwhile; // End of the loop. ?>
	</div>
<?php get_footer(); ?>