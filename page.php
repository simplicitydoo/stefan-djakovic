<?php
/**
 * The template for displaying all single page
 *
 */

get_header(); ?>
	<div class="container-small page-single-post">
		<?php while ( have_posts() ) : the_post(); ?>
			<div class="text-center">

				<?php if ( has_post_thumbnail() ) : ?>
	        		<?php the_post_thumbnail(); ?>
	    		<?php endif; ?>

	    	</div>	
			<h1 class="text-center project-title"><?php the_title(); ?></h1>
			
			<?php the_content(); ?>

				

		<?php endwhile; // End of the loop. ?>
	</div>
<?php get_footer(); ?>