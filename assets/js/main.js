jQuery(document).ready(function($){

	'use strict';

	if ( $('.fancybox').length ) {
		$(".fancybox").fancybox({
			autoCenter	: true,
			beforeShow: function() {
				var title = this.element[0].getAttribute('data-title');
				var description = this.element[0].getAttribute('data-description');
				var link = this.element[0].getAttribute('data-link');

				if ( link == 0 ) {
					var customContent = "<div class='wrap-caption'><div class='caption'><div class='text-wrap'><h3>" + title + "</h3><p>" + description + "</p></div><div class='button-wrap'></div></div></div>";
					this.title = this.title ? this.title + customContent : customContent;
				} 
				else {
					var customContent = "<div class='wrap-caption'><div class='caption'><div class='text-wrap'><h3>" + title + "</h3><p>" + description + "</p></div><div class='button-wrap'><a class='project-button' href='"+ link +"'><span class='icon-copy'></span> Open this project</a></div></div></div>";
					this.title = this.title ? this.title + customContent : customContent;
				}

		    }
		});
	}

	if ( $('.stefandjakovic_post_container').length ) {
		$('.stefandjakovic_post_container').imagesLoaded(function() {
			$('.stefandjakovic_post_container').masonry({
				itemSelector: '.masonry-item'
			});
		});
	}

	$('.sub-menu').hide();
	

	$(".menu-item-has-children").hover( function() {
		$('.sub-menu').slideToggle()
		$(this).toggleClass('nav-hover-color');
	});

	$('.child').slideDown();

	/**
	 * AJAX Overview
	 */
	if ( $('.stefandjakovic_post_container').length ) {

		$(document).on('click', '.loadmore-button', function(event) {

			// Prevents the default behaviour of the button.
			event.preventDefault();

			// Make sure that the user can't click the button multiple times unless
			// AJAX call is finished.
			var anchor = $(this);
			if ( anchor.data('disabled') ) {

				return false;

			}
			anchor.data('disabled', 'disabled');;

			// Let's get some basic variables here that we're going to need.
			var $this = $(this),
				nonce = this.getAttribute('data-nonce'),
				offset = this.getAttribute('data-offset'),
				text = $this.text(),
				$container = $('.stefandjakovic_post_container');

			// Let's do some AJAX
			$.ajax({
				type: 'post',
				url: stefan_ajax.ajax_url,
				data: {
					'nonce': nonce,
					'offset': offset,
					'action': 'load_more_overview'
				},
				beforeSend: function() {
					$this.text( stefan_ajax.button );
				},
				success: function(data) {
					if ( data.has_more == true ) {
						var $html = $.parseJSON( data.html );
						$container.append( $html );
						$('.stefandjakovic_post_container').imagesLoaded(function() {
							$('.stefandjakovic_post_container').masonry( 'reloadItems' );
							$('.stefandjakovic_post_container').masonry( 'layout' );
						});
						$this.attr('data-offset', data.offset);

						$this.text( text );
						anchor.removeData('disabled');
					} else {
						console.log(data.message);
						$this.text( data.message );
						$this.attr( 'disabled', 'disabled' );
					}
				},
				error: function(error) {
					console.log(error);
				}
			});

		});

	}

});