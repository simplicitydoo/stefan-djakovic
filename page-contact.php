<?php
/**
 * Template Name: Contact Page
 *
 * @package WordPress
 * @subpackage stefandjakovic
 * @since stefandjakovic 1.0
 */
?>

<?php get_header(); ?>
	<div class="container-small page-contact">
		<?php while ( have_posts() ) : the_post(); ?>
			<h1 class="text-center"><?php the_title(); ?></h1>
			<div class="row">
				<div class="col-md-12">
					<h3 class="text-center"><?php _e( 'Shoot me an e-mail, or give me a call:', 'stefandjakovic' ); ?></h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 contact-mobile">
					<h3><span class="contact-info">Cellphone:</span> <?php echo the_field('phone_number', 'option'); ?></h3>
				</div>
				<div class="col-md-6 skype">
					<h3><span class="contact-info">Skype:</span> <?php echo the_field('skype_username', 'option'); ?></h3>
				</div>
			</div>

			<?php the_content(); ?>

				

		<?php endwhile; ?>
	</div>
<?php get_footer(); ?>