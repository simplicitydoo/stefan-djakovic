<div class="alignright social-top">
	<a href="<?php echo the_field('facebook', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/fb.svg" alt="Facebook"></a>
	<a href="<?php echo the_field('twitter', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tw.svg" alt="Twitter"></a>
	<a href="<?php echo the_field('instagram', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/insta.svg" alt="Instagram"></a>
	<a href="<?php echo the_field('vsco', 'option'); ?>" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/vsco.svg" alt="Vsco"></a>
</div>