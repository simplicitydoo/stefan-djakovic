<?php

			// vars
			$title = get_sub_field('overview_title');
			$image = get_sub_field('overview_image');
			$description = get_sub_field('overview_short_description');
			$link = get_sub_field('link_to_the_project');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="item">

		<div class="image-container">
			<a href="<?php echo $image['url']; ?>" class="fancybox" rel="gallery1" data-title="<?php echo esc_attr($title); ?>" data-description="<?php echo esc_attr($description); ?>" data-link="<?php echo esc_attr($link); ?>">
				<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
			</a>
		</div>

	</div>
</article>