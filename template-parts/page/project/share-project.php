<div class="row share-box-outter">
	<div class="col-md-12 text-center">
		<h3><?php _e('SHARE THIS PROJECT', 'stefandjakovic'); ?></h3>
	</div>
	<div class="col-md-12 text-center share-box">
		<a href="http://www.facebook.com/share.php?u=<?php the_permalink(); ?>&title=<?php the_title(); ?>" title="Share on Facebook." target="_blank"><span class="icon-facebook"></a>
		<a href="http://twitter.com/home/?status=<?php the_title(); ?> - <?php the_permalink(); ?>" title="Tweet this!" target="_blank"><span class="icon-twitter"></a>
		<a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; ?>" target="_blank"><span class="icon-_526"></a>
		<a href="http://www.tumblr.com/share?v=3&u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank"><span class="icon-tumblr"></a>
	</div>
</div>